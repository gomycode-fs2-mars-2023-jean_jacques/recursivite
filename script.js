function Palindrome(mot) {
  if (mot.length === 0 || mot.length === 1) {
    return true;
  }
  if (mot[0] === mot[mot.length - 1]) {
    return Palindrome(mot.slice(1, mot.length - 1));
  } else {
    return false;
  }
}